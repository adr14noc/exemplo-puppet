package {'wget':
   ensure => latest,
}
exec {'enable jenkins repo':
     command => '/usr/bin/wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo',
}

exec {'update repo':
     command => '/usr/bin/dnf update -y',
}

exec {'import key':
     command => '/usr/bin/rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key',
}

package {'jenkins':
  ensure => latest,
}

exec {'start service':
     command => '/usr/bin/systemctl start jenkins',
}

exec {'Root Passwdord':
     command => '/usr/bin/cat /var/lib/jenkins/secrets/initialAdminPassword',
}
